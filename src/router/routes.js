
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { 
        path: '', 
        component: () => import('pages/movies.vue') ,
        name : 'movies'
      },
      {
        path : '/create',
        component : () => import('pages/create.vue'),
        name : 'createMovie'
      },
      {
        path : '/edit',
        component : () => import('pages/edit.vue'),
        name : 'editMovie'
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
