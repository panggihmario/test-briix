import { defineStore } from "pinia";

export const useMovie = defineStore('movie', {
  state : () => ({
    movies : [],
    movie : {},
    initMovies : []
  }),
  actions : {
    addMovie (payload) {
      this.movies.push(payload)
      this.initMovies.push(payload)
    },
    setDataMovie(data) {
      this.movie = data
    },
    editMovie(data) {
      const moviesData = this.movies
      const updating = moviesData.map(mo => {
        if(data.id === mo.id) {
          return {
            ...data
          }
        }else{
          return {
            ...mo
          }
        }
      })
      this.movies = updating
      this.initMovies = updating
    },
    deleteMovie (id) {
      const moviesData = this.movies
      const indexId = moviesData.findIndex(data => data.id === id)
      moviesData.splice(indexId, 1)
      this.movies = moviesData
      this.initMovies = moviesData
    },
    searchMovie (keyword) {
      const filterKeyword = keyword.toUpperCase()
      const init = this.initMovies
      const moviesData = this.movies
      const result = init.filter(movie => {
        if(movie.title.toUpperCase().indexOf(filterKeyword) > -1) {
          return movie
        }
      })
      this.movies = result
    }
  }
})